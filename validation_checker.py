#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 24 11:27:03 2017

@author: binh
"""
import cv2


# Constant
RED = (0, 0, 255)
GREEN = (0, 255,0)
BLUE = (255, 0, 0)


class Detector(object):

    def __init__(self, haar):
        self.detector = cv2.CascadeClassifier(haar)


class FaceDetector(Detector):

    def detect(self, img):
        return self.detector.detectMultiScale(img, 1.1, 3)


class EyeDetector(Detector):

    def detect(self, face_img):
        _, w = face_img.shape
        min_size = (int(w / 5), int(w / 5))
        return self.detector.detectMultiScale(face_img, 1.1,1)


class MouthDetector(Detector):

    def detect(self, face_img):
        h, w = face_img.shape
        ratio = 1/3
        min_size = (int(ratio*w), 5)
        return self.detector.detectMultiScale(face_img, 1.1, 3, minSize=min_size)


def get_face_bound(face, gray, color):
    x, y, w, h = face
    face_gray = gray[y:y + h, x:x + w]
    face_color = color[y:y + h, x:x + w]
    return face_gray, face_color


def get_lower_face(face, gray, img):
    x, y, w, h = face
    lower_face_gray = gray[y + 2 * int(h / 3):y + h, x:x + w]
    lower_face_color = img[y + 2 * int(h / 3):y + h, x:x + w]
    return lower_face_gray, lower_face_color


def draw_box(image, shape, color, width=2):
    x, y, w, h = shape
    cv2.rectangle(image, (x, y), (x + w, y + h), color, width)


def put_text_on_image(img, content):
    cv2.putText(img, content, (20, 100), cv2.FONT_HERSHEY_TRIPLEX, 2, (0, 255, 0))


class ValidationChecker(object):
    def __init__(self):
        self.face_detector = FaceDetector('opencv-haar/haarcascade_frontalface_alt.xml')
        self.eye_detector = EyeDetector('opencv-haar/haarcascade_eye_tree_eyeglasses.xml')
        self.mouth_detector = MouthDetector('opencv-haar/Mouth.xml')

    def check(self, img):
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        gray = cv2.equalizeHist(gray)
        faces = self.face_detector.detect(gray)
        if len(faces) == 0:
            return False
        for face in faces:
            draw_box(img, face, RED)
            roi_face_gray, roi_face_color = get_face_bound(face, gray, img)
            eyes = self.eye_detector.detect(roi_face_gray)
            if len(eyes) < 2:
                return False
            for eye in eyes:
                draw_box(roi_face_color, eye, GREEN)
            lower_face_gray, lower_face_color = get_lower_face(face, gray, img)
            mouths = self.mouth_detector.detect(lower_face_gray)
            if len(mouths) < 1:
                return False
            for mouth in mouths:
                draw_box(lower_face_color, mouth, BLUE)
        return True
