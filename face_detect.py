import sys
import cv2
from validation_checker import *


checker = ValidationChecker()
img_path = sys.argv[1]
img = cv2.imread(img_path)
res = checker.check(img)
put_text_on_image(img, str(res))
cv2.imshow('Image', img)
cv2.waitKey(0)